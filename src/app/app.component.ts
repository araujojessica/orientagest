import { Component, ViewChildren, QueryList  } from '@angular/core';
import { Platform, NavController, ActionSheetController,IonRouterOutlet } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { Pages } from './interfaces/pages';

import { Router } from '@angular/router';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public appPages: Array<Pages>;
  
  lastTimeBackPress = 0;
  timePeriodToExit = 2000;
 @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;
  
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private actionSheetCtrl: ActionSheetController,
	  private router: Router,
    public navCtrl: NavController
  ) {
    this.appPages = [
      {
        title: 'Home',
        url: '/sintoma',
        direct: 'root',
        icon: 'home'
      }
    ];

  
    this.initializeApp();
	this.statusBar.backgroundColorByHexString('#ffffff');
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
	  this.statusBar.backgroundColorByHexString('#ffffff');
      this.splashScreen.hide();
    }).catch(() => {});
  }

  goToEditProgile() {
    this.navCtrl.navigateForward('edit-profile');
  }

  logout() {
   // window.localStorage.removeItem('nome');
      this.router.navigate(['']);
   // navigator['app'].exitApp();
    
  }
  
  // active hardware back button
    backButtonEvent() {
		
        this.platform.backButton.subscribe(async () => {
            // close action sheet
            try {
                const element = await this.actionSheetCtrl.getTop();
                if (element) {
                    element.dismiss();
                    return;
                }
            } catch (error) {
            }

            

            this.routerOutlets.forEach((outlet: IonRouterOutlet) => {
                if (outlet && outlet.canGoBack()) {
                    outlet.pop();

                } else if (this.router.url === '/home') {
                    if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
                        // this.platform.exitApp(); // Exit from app
                        navigator['app'].exitApp(); // work in ionic 4

                    } else {
                        
                        this.lastTimeBackPress = new Date().getTime();
                    }
                }
            });
        });
    }

  referencias(){
    this.router.navigateByUrl('/referencias');
  }

  autores(){
    this.router.navigateByUrl('/autores');
  }

}
