import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '',loadChildren: './pages/home/home.module#HomePageModule'},
  { path: 'sintoma', loadChildren: './pages/sintomas/sintoma.module#SintomaPageModule'},
  { path: 'cabeca', loadChildren: './pages/cabeca/cabeca.module#CabecaPageModule'},
  { path: 'mamas', loadChildren: './pages/mamas/mamas.module#MamasPageModule'},
  { path: 'digestorio', loadChildren: './pages/digestorio/digestorio.module#DigestorioPageModule'},
  { path: 'urinario', loadChildren: './pages/urinario/urinario.module#UrinarioPageModule'},
  { path: 'pele', loadChildren: './pages/pele/pele.module#PelePageModule'},
  { path: 'genital', loadChildren: './pages/genital/genital.module#GenitalPageModule'},
  { path: 'postura', loadChildren: './pages/postura/postura.module#PosturaPageModule'},
  { path: 'referencias', loadChildren: './pages/referencias/referencias.module#ReferenciasPageModule'},
  { path: 'autores', loadChildren: './pages/autores/autores.module#AutoresPageModule'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
