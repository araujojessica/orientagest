import { Component, OnInit } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,
  
  ModalController } from '@ionic/angular';
  
import {Router} from '@angular/router';

import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './postura.page.html',
  styleUrls: ['./postura.page.scss']
})
export class PosturaController implements OnInit {
    constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
   
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private router: Router,
    public toastCtrl: ToastController,
    private formBuilder: FormBuilder
  ) {
    
    
  }
    ngOnInit(): void {
    }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }
  
  togglePostura() {
    if (document.getElementById("postura").style.display == 'none') {
     document.getElementById("postura").style.display = "block";
    } else {
     document.getElementById("postura").style.display = "none";
    }
  }

  async sintomasPostura() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'sintomas',
      subHeader: '',
      message: '• O peso adicional da gravidez (útero/ feto e anexos), desestabiliza o equilíbrio materno. A gestante assume uma atitude involuntária de curvatura lombar. Esta postura modifica sua marcha. O andar oscilante, com passos curtos e lentos',
      buttons: ['Fechar']
    });

    await alert.present();
  }
  async sintomasPosturaConduta() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'conduta',
      subHeader: '',
      message: '• Evite longos períodos em uma mesma posição<br>• Faça atividades físicas sem impacto, como alongamentos, pilates, hidroginástica, natação, ioga, drenagens/ massagens relaxantes e fisioterapia pélvica<br>• Opte por sapatos confortáveis e que tenham um pequeno salto ou palmilha<br>• Tente sentar ou deitar-se confortavelmente colocando um travesseiro entre as pernas e que mantenha a coluna e postura reta<br>• Não podem ser tomados anti-inflamatórios',
      buttons: ['Fechar']
    });

    await alert.present();
  }
  async sintomasPosturaAlerta() {
    const alert = await this.alertCtrl.create({
      header: 'Sinais de alerta:',
      cssClass: 'alerta',
      subHeader: '',
      message: '• Dor na coluna ou na região das costas ao simples movimento como levantar ou sentar<br>• Quando não conseguir se locomover',
      buttons: ['Fechar']
    });

    await alert.present();
  }
}