import { Component, OnInit } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,  
  ModalController } from '@ionic/angular';
  
import {Router} from '@angular/router';

import {FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './sintoma.page.html',
  styleUrls: ['./sintoma.page.scss']
})
export class SintomaController implements OnInit {
  usuario:string;
    constructor(
      public navCtrl: NavController,
      public menuCtrl: MenuController,
    
      public alertCtrl: AlertController,
      public modalCtrl: ModalController,
      private router: Router,
      public toastCtrl: ToastController
  ) {
    
    
  }
    ngOnInit(): void {

      this.usuario = window.localStorage.getItem('nome');
     
    }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }

  cabeca() {
    this.router.navigateByUrl('/cabeca');
  }

  mamas() {
    this.router.navigateByUrl('/mamas');
  }

  digestorio(){
    this.router.navigateByUrl('/digestorio');
  }

  urinario(){
    this.router.navigateByUrl('/urinario');
  }

  pele(){
    this.router.navigateByUrl('/pele');
  }

  genital(){
    this.router.navigateByUrl('/genital');
  }

  postura() {
    this.router.navigateByUrl('/postura');
  }
}