import { Component, OnInit } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,
  
  ModalController } from '@ionic/angular';
  
import {Router, NavigationExtras} from '@angular/router';

import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss']
})
export class HomePage  implements OnInit{
  public onLoginForm: FormGroup;
  nome : string; 
  nome1: string;
   constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private router: Router,
    public toastCtrl: ToastController,
    private formBuilder: FormBuilder
  ) {
    
    
  }
  ngOnInit() {
    this.onLoginForm = this.formBuilder.group({
      'userName': [null, Validators.compose([Validators.required])],
      'weekGest': ['']
    });

    window.localStorage.getItem(this.nome);
 
  }


  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }

  async goToHome(){

  
    const userName = this.onLoginForm.get('userName').value;
    const week = this.onLoginForm.get('weekGest').value;
    window.localStorage.setItem('nome', this.onLoginForm.get('userName').value);
    
    if(week == 2){
      let toast = this.toastCtrl.create({
        message: 'Orientações não recomendadas para gestantes com mais de 13 semanas.',
        duration: 4000,
        position: 'middle',
        color: 'danger'
      });
  
      (await toast).present();
    }
    //this.onLoginForm.get('userName').reset();

    this.router.navigateByUrl('/sintoma');
    

  }

}
