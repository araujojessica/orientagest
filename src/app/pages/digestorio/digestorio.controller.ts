import { Component, OnInit } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,
  
  ModalController } from '@ionic/angular';
  
import {Router} from '@angular/router';

import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { setClassMetadata } from '@angular/core/src/render3';

@Component({
  selector: 'app-home',
  templateUrl: './digestorio.page.html',
  styleUrls: ['./digestorio.page.scss']
})
export class DigestorioController implements OnInit {
    constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
   
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private router: Router,
    public toastCtrl: ToastController,
    private formBuilder: FormBuilder
  ) {
    
    
  }
    ngOnInit(): void {
    }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }

  toggleDoencas() {
    if (document.getElementById("doencas").style.display == 'none') {
     document.getElementById("doencas").style.display = "block";
    } else {
     document.getElementById("doencas").style.display = "none";
    }
  }
  
  toggleVolume() {
    if (document.getElementById("volume").style.display == 'none') {
     document.getElementById("volume").style.display = "block";
    } else {
     document.getElementById("volume").style.display = "none";
    }
  }

  toggleDistensao() {
    if (document.getElementById("distensao").style.display == 'none') {
     document.getElementById("distensao").style.display = "block";
    } else {
     document.getElementById("distensao").style.display = "none";
    }
  }

  toggleNauseas() {
    if (document.getElementById("nauseas").style.display == 'none') {
     document.getElementById("nauseas").style.display = "block";
    } else {
     document.getElementById("nauseas").style.display = "none";
    }
  }

  toggleVomitos() {
    if (document.getElementById("vomitos").style.display == 'none') {
     document.getElementById("vomitos").style.display = "block";
    } else {
     document.getElementById("vomitos").style.display = "none";
    }
  }

  async sintomasVolume() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'sintomas',
      subHeader: '',
      message: '• Aumento do útero juntamente com a barriga que pode gerar dores em baixo ventre, conforme a barriga vai aumentando',
      buttons: ['Fechar']
    });

    await alert.present();
  }

  async sintomasVolumeAlerta() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'alerta',
      subHeader: '',
      message: '• Se cólica forte ou dor em baixo ventre de forte intensidade deve se procurar o hospital',
      buttons: ['Fechar']
    });

    await alert.present();
  }

  async sintomasDistensao() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'sintomas',
      subHeader: '',
      message: '• Redução do trânsito intestinal devido aos hormônios da gestação, que faz com que os músculos intestinais se relaxem e permanecem por mais tempo, o normal é até no máximo 5 dias sem ir ao banheiro<br>• Pode haver piora pelo uso do sulfato ferroso<br>• Observe a presença de hemorróidas',
      buttons: ['Fechar']
    });

    await alert.present();
  }
  async sintomasDistensaoConduta() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'conduta',
      subHeader: '',
      message: '• Alimente-se com uma dieta rica em fibras, como: frutas, legumes e verduras<br>• Beba bastante água',
      buttons: ['Fechar']
    });

    await alert.present();
  }
  async sintomasDistensaoAlerta() {
    const alert = await this.alertCtrl.create({
      header: 'Sinais de alerta:',
      cssClass: 'alerta',
      subHeader: '',
      message: '• Dor ao evacuar<br>• Mais do que 5 (cinco) dias sem ir ao banheiro<br>• Sangramento pelo ânus',
      buttons: ['Fechar']
    });

    await alert.present();
  }

  async sintomasNauseas() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'sintomas',
      subHeader: '',
      message: '• Principal sintoma no início da gestação, devido às alterações hormonais<br>• Comumente aparecem entre 6ª e 14ª semanas de gestação<br>• Tipicamente aparecem no período da manhã, devido ao maior tempo noturno com o estômago vazio<br>• Podem ser acompanhadas de vômitos<br>• Tendem a melhorar ao longo do dia, mas podem ocorrer a qualquer hora do dia<br>• As náuseas podem ser mais intensas ou potencializadas por algumas situações orgânicas ou psicológicas (gestação de gêmeos, insegurança materna , ansiedade, hipertireoidismo)',
      buttons: ['Fechar']
    });

    await alert.present();
  }
  async sintomasNauseasConduta() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'conduta',
      subHeader: '',
      message: '• Evite longos períodos sem se alimentar<br>• Fracione as refeições<br>• Opte por alimentos mais leves, como frutas e fibras, evitando carboidratos e doces<br>• Tente acalmar-se, optando por lugares mais calmos e sem aglomerações<br>• Faça uma boa noite de sono<br>• Tome as outras medicações prescritas pelo seu obstetra corretamente<br>• Beba bastante água',
      buttons: ['Fechar']
    });

    await alert.present();
  }

  async sintomasVomitos() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'sintomas',
      subHeader: '',
      message: '• As principais causas são fatores endócrinos, autoimunes e fatores psicológicos<br>• Ocorrem devido a persistência das náuseas.<br>• Acometem mais “mamães de primeira viagem”, jovens, obesas, gestação de gêmeos, gestação de mola hidatiforme, ou gestantes com história de vômitos em gestação anterior',
      buttons: ['Fechar']
    });

    await alert.present();
  }
  async sintomasVomitoConduta() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'conduta',
      subHeader: '',
      message: '• Evite longos períodos sem se alimentar<br>• Fracione as refeições<br>• Opte por alimentos mais leves, como frutas e fibras, evitando carboidratos e doces<br>• Tente acalmar-se, optando por lugares mais calmos e sem aglomerações<br>• Faça uma boa noite de sono<br>• Tome as outras medicações prescritas pelo seu obstetra corretamente<br>• Beba bastante água',
      buttons: ['Fechar']
    });

    await alert.present();
  }
  async sintomasVomitosAlerta() {
    const alert = await this.alertCtrl.create({
      header: 'Sinais de alerta:',
      cssClass: 'alerta',
      subHeader: '',
      message: '• Fraqueza<br>• Sede<br>• Desidratação<br>• Pele seca/ Boca seca<br>• Quando estiver urinando pouca quantidade<br>• Quando não estiver alimentando-se corretamente<br>• Convulsão e delírios',
      buttons: ['Fechar']
    });

    await alert.present();
  }

  async sintomasDoencas() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'sintomas',
      subHeader: '',
      message: '• A hiperemia ou vermelhidão gengival ocorre em 80% das gestantes<br>• Gengiva que sangra facilmente durante a escovação ou ao uso do fio dental devido aos hormônios da gravidez<br>• Gengiva inchada ou vermelha<br>• Mau hálito persistente<br>• Dores agudas ao mastigar os alimentos',
      buttons: ['Fechar']
    });

    await alert.present();
  }
  async sintomasDoencasConduta() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'conduta',
      subHeader: '',
      message: '• Escove os dentes e use corretamente o fio dental para remoção de placas e restos de alimentos<br>• Alimente corretamente para garantir nutrição adequada<br>• Evite cigarros e outras formas de tabaco<br>• Visite o dentista regularmente',
      buttons: ['Fechar']
    });

    await alert.present();
  }
  async sintomasDoencasAlerta() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'alerta',
      subHeader: '',
      message: '• Gengivite: primeiro estágio da doença- inflamação da gengiva, causada pela placa bacteriana (vermelhidão, inchaço e sangramento durante a escovação)<br>• Periodontite: o osso de apoio e as fibras que seguram os dentes no lugar estão irreversivelmente danificados<br>• Periodontite avançada: as fibras e o osso dos dentes estão sendo destruídos, fazendo com que o dente fique mole ou movimente. Pode afetar a mordida e a forma como se alimenta ou se comunica. Deve se tentar salvar os dentes afetados ou ter que extraí-los. Qualquer procedimento que necessite de anestesia local deverá ser feito após o 5º mês.',
      buttons: ['Fechar']
    });

    await alert.present();
  }

}