import { Component, OnInit } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,
  
  ModalController } from '@ionic/angular';
  
import {Router} from '@angular/router';

import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './mamas.page.html',
  styleUrls: ['./mamas.page.scss']
})
export class MamasController implements OnInit {
    constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
   
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private router: Router,
    public toastCtrl: ToastController,
    private formBuilder: FormBuilder
  ) {
    
    
  }
    ngOnInit(): void {
    }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }

  toggleMamas() {
    if (document.getElementById("mamas").style.display == 'none') {
     document.getElementById("mamas").style.display = "block";
    } else {
     document.getElementById("mamas").style.display = "none";
    }
  }
  
  async sintomasMamas() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'sintomas',
      subHeader: '',
      message: '• Na 5ª semana de gestação ocorre aumento das camadas de gordura da pele e desenvolvimento dos ductos mamários o que pode gerar dor ou desconforto nas mamas',
      buttons: ['Fechar']
    });

    await alert.present();
  }
  async sintomasMamasConduta() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'conduta',
      subHeader: '',
      message: '• Não faça expressão dos mamilos continuamente<br>• Utilize sutiã com alças firmes e confortáveis<br>• Palpe e conheça suas mamas<br>',
      buttons: ['Fechar']
    });

    await alert.present();
  }

  async sintomasMamasAlerta() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'alerta',
      subHeader: '',
      message: '• Nódulo palpável<br>• Secreção com sangue<br>• Alteração da pele ou formato dos mamilos',
      buttons: ['Fechar']
    });

    await alert.present();
  }

}