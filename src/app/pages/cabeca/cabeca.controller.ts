import { Component, OnInit } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,
  
  ModalController } from '@ionic/angular';
  
import {Router} from '@angular/router';

import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './cabeca.page.html',
  styleUrls: ['./cabeca.page.scss']
})
export class CabecaController implements OnInit {
    constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
   
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private router: Router,
    public toastCtrl: ToastController,
    private formBuilder: FormBuilder
  ) {
    
    
  }
    ngOnInit(): void {
    }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }

  toggleSalivacao() {
    if (document.getElementById("salivacao").style.display == 'none') {
     document.getElementById("salivacao").style.display = "block";
    } else {
     document.getElementById("salivacao").style.display = "none";
    }
  }

  toggleTontura() {
    if (document.getElementById("tontura").style.display == 'none') {
     document.getElementById("tontura").style.display = "block";
    } else {
     document.getElementById("tontura").style.display = "none";
    }
  }

  toggleSonolencia() {
    if (document.getElementById("sonolencia").style.display == 'none') {
     document.getElementById("sonolencia").style.display = "block";
    } else {
     document.getElementById("sonolencia").style.display = "none";
    }
  }

  toggleAlteracoes() {
    if (document.getElementById("alteracoes").style.display == 'none') {
     document.getElementById("alteracoes").style.display = "block";
    } else {
     document.getElementById("alteracoes").style.display = "none";
    }
  }

  

    async sintomasSalivacao() {
      const alert = await this.alertCtrl.create({
        header: '',
        cssClass: 'sintomas',
        subHeader: '',
        message: '• Aumento da produção de saliva',
        buttons: ['Fechar']
      });
  
      await alert.present();
    }
    async sintomasSalivacaoConduta() {
      const alert = await this.alertCtrl.create({
        header: '',
        cssClass: 'conduta',
        subHeader: '',
        message: '• Evite longos períodos sem alimentar-se<br>• Beba bastante água',
        buttons: ['Fechar']
      });
  
      await alert.present();
    }

    async sintomasTontura() {
      const alert = await this.alertCtrl.create({
        header: '',
        cssClass: 'sintomas',
        subHeader: '',
        message: '• Acontece devido às mudanças na circulação do sangue e devido às alterações na pressão arterial<br>• Piora quando a paciente está na posição de pé e melhora quando está deitado e principalmente para o lado esquerdo',
        buttons: ['Fechar']
      });
  
      await alert.present();
    }
    async sintomasTonturaConduta() {
      const alert = await this.alertCtrl.create({
        header: '',
        cssClass: 'conduta',
        subHeader: '',
        message: '• Evite longos períodos sem alimentar-se<br>• Beba bastante água<br>• Não faça mudanças bruscas de posição<br>• Controle o estresse e a ansiedade<br>• Evite longos períodos na posição sentada ou em pé, ou lugares aglomerados<br>• Eleve os pés ao longo do dia',
        buttons: ['Fechar']
      });
  
      await alert.present();
    }
    async sintomasTonturaAlerta() {
      const alert = await this.alertCtrl.create({
        header: 'Sinais de alerta:',
        cssClass: 'alerta',
        subHeader: '',
        message: '<h3>SINAIS DE ALERTA:</h3><br>• Tontura persistente<br>• Associada a outros sintomas, como a dor de cabeça, batedeira no coração, vômitos, febre, convulsão',
        buttons: ['Fechar']
      });
  
      await alert.present();
    }

    async sintomasSonolencia() {
      const alert = await this.alertCtrl.create({
        header: '',
        cssClass: 'sintomas',
        subHeader: '',
        message: '• Acontecem devido ao hormônio que aparece no início da gestação<br>• Sintomas de estresse, hábitos de vida inadequados podem piorar o cansaço',
        buttons: ['Fechar']
      });
  
      await alert.present();
    }
    async sintomasSonolenciaConduta() {
      const alert = await this.alertCtrl.create({
        header: '',
        cssClass: 'conduta',
        subHeader: '',
        message: '• Faça atividades físicas com a liberação do seu médico<br>• Beba bastante água<br>• Tenha uma dieta equilibrada e saudável<br>• Controle o estresse e a ansiedade<br>• Faça drenagem linfática e massagem relaxante<br>• Sempre que puder tente descansar<br>• Tenha uma boa noite de sono',
        buttons: ['Fechar']
      });
  
      await alert.present();
    }
    async sintomasSonolenciaAlerta() {
      const alert = await this.alertCtrl.create({
        header: 'Sinais de alerta:',
        cssClass: 'alerta',
        subHeader: '',
        message: '<h3>SINAIS DE ALERTA:</h3><br>• Falta de ar<br>• Associado a outros sintomas, como dificuldade de executar atividades diárias<br>• Indisposição de forte intensidade e constante',
        buttons: ['Fechar']
      });
  
      await alert.present();
    }

    async sintomasAlteracoes() {
      const alert = await this.alertCtrl.create({
        header: '',
        cssClass: 'sintomas',
        subHeader: '',
        message: '• Alterações de humor<br>• Choro fácil<br>• Depressão<br>• Diminuição do apetite',
        buttons: ['Fechar']
      });
  
      await alert.present();
    }
    async sintomasAlteracoesConduta() {
      const alert = await this.alertCtrl.create({
        header: '',
        cssClass: 'conduta',
        subHeader: '',
        message: '• Solicite AJUDA<br>• Avise aos familiares e ao médico obstetra<br>• Tente controlar o estresse e a ansiedade<br>• Tenha uma boa noite de sono',
        buttons: ['Fechar']
      });
  
      await alert.present();
    }
    async sintomasAlteracoesAlerta() {
      const alert = await this.alertCtrl.create({
        header: 'Sinais de alerta:',
        cssClass: 'alerta',
        subHeader: '',
        message: '<h3>SINAIS DE ALERTA:</h3><br>• Desejo de morte ou suicídio<br>• Rejeição da gravidez<br>• Desejo de realizar aborto',
        buttons: ['Fechar']
      });
  
      await alert.present();
    }

    



}
