import { Component, OnInit } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,
  
  ModalController } from '@ionic/angular';
  
import {Router} from '@angular/router';

import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './genital.page.html',
  styleUrls: ['./genital.page.scss']
})
export class GenitalController implements OnInit {
    constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
   
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private router: Router,
    public toastCtrl: ToastController,
    private formBuilder: FormBuilder
  ) {
    
    
  }
    ngOnInit(): void {
    }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }
  
  toggleTonalidade() {
    if (document.getElementById("tonalidade").style.display == 'none') {
      document.getElementById("tonalidade").style.display = "block";

    } else {
     document.getElementById("tonalidade").style.display = "none";
    }
  }

  toggleSecrecao() {
    if (document.getElementById("secrecao").style.display == 'none') {
     document.getElementById("secrecao").style.display = "block";
    } else {
     document.getElementById("secrecao").style.display = "none";
    }
  }

  async sintomasTonalidade() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'sintomas',
      subHeader: '',
      message: '• Escurecimento da vulva e da vagina, devido ao aumento dos hormônios na gravidez',
      buttons: ['Fechar']
    });

    await alert.present();
  }
  async sintomasTonalidadeAlerta() {
    const alert = await this.alertCtrl.create({
      header: 'Sinais de alerta:',
      cssClass: 'alerta',
      subHeader: '',
      message: '• Presença de feridas ou verrugas<br>• Sangramento<br>• Coceira',
      buttons: ['Fechar']
    });

    await alert.present();
  }

  async sintomasSecrecao() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'sintomas',
      subHeader: '',
      message: '• Aumento na quantidade de muco/ corrimento devido a alteração do Ph vaginal',
      buttons: ['Fechar']
    });

    await alert.present();
  }
  async sintomasSecrecaoConduta() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'conduta',
      subHeader: '',
      message: '• Use calcinhas de algodão<br>• Use roupas frescas<br>• Mantenha alimentação correta<br>• Faça uma boa higiene íntima',

      buttons: ['Fechar']
    });

    await alert.present();
  }
  async sintomasSecrecaoAlerta() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'alerta',
      subHeader: '',
      message: '• Corrimento com odor fétido<br>• Mudança na cor<br>• Sangramentos<br>• Coceira<br>• Saída de líquido',
      buttons: ['Fechar']
    });

    await alert.present();
  }

}