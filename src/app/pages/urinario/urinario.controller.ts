import { Component, OnInit } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,
  
  ModalController } from '@ionic/angular';
  
import {Router} from '@angular/router';

import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './urinario.page.html',
  styleUrls: ['./urinario.page.scss']
})
export class UrinarioController implements OnInit {
    constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
   
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private router: Router,
    public toastCtrl: ToastController,
    private formBuilder: FormBuilder
  ) {
    
    
  }
    ngOnInit(): void {
    }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }
  
  toggleFrequencia() {
    if (document.getElementById("frequencia").style.display == 'none') {
     document.getElementById("frequencia").style.display = "block";
    } else {
     document.getElementById("frequencia").style.display = "none";
    }
  }

  async sintomasFrequencia() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'sintomas',
      subHeader: '',
      message: '• Ocorre aumento da produção total de urina, devido ao útero em crescimento ser empurrado pra frente, promovendo a compressão da bexiga.',
      buttons: ['Fechar']
    });

    await alert.present();
  }
  async sintomasFrequenciaConduta() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'conduta',
      subHeader: '',
      message: '• Evite beber líquidos antes de dormir<br>• Evite longos períodos sem ir ao banheiro<br>• Após a 12ª semana, esse sintoma diminui devido ao crescimento do útero em direção à cavidade abdominal e depois volta em torno da 35ª semana.',
      buttons: ['Fechar']
    });

    await alert.present();
  }
  async sintomasFrequenciaAlerta() {
    const alert = await this.alertCtrl.create({
      header: 'Sinais de alerta:',
      cssClass: 'alerta',
      subHeader: '',
      message: '• Dor ao urinar<br>• Urina com odor fétido<br>• Dificuldade para urinar<br>• Dor nas costas<br>• Associada a febre, fraqueza, desidratação, calafrios',
      buttons: ['Fechar']
    });

    await alert.present();
  }

}