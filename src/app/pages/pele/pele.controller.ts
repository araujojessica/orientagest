import { Component, OnInit } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,
  
  ModalController } from '@ionic/angular';
  
import {Router} from '@angular/router';

import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './pele.page.html',
  styleUrls: ['./pele.page.scss']
})
export class PeleController implements OnInit {
    constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
   
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private router: Router,
    public toastCtrl: ToastController,
    private formBuilder: FormBuilder
  ) {
    
    
  }
    ngOnInit(): void {
    }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }
  
  toggleEstrias() {
    if (document.getElementById("estrias").style.display == 'none') {
     document.getElementById("estrias").style.display = "block";
    } else {
     document.getElementById("estrias").style.display = "none";
    }
  }

  toggleAlteracoesP() {
    if (document.getElementById("alteracoesp").style.display == 'none') {
     document.getElementById("alteracoesp").style.display = "block";
    } else {
     document.getElementById("alteracoesp").style.display = "none";
    }
  }

  async sintomasEstrias() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'sintomas',
      subHeader: '',
      message: '• Devido ao estiramento da pele juntamente à diminuição da hidratação, ganho de peso acentuado e à genética<br>• Podem aparecer em áreas predispostas como abdome, mamas, nádegas e coxas',
      buttons: ['Fechar']
    });

    await alert.present();
  }
  async sintomasEstriasConduta() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'conduta',
      subHeader: '',
      message: '• Beba bastante água<br>• Evite ganhar muito peso durante a gravidez<br>• Pratique atividades físicas<br>• Mantenha a pele hidratada',
      buttons: ['Fechar']
    });
    
    

    await alert.present();
  }

  async sintomasEstriasAlerta() {
    const alert = await this.alertCtrl.create({
      header: 'Sinais de alerta:',
      cssClass: 'alerta',
      subHeader: '',
      message: '• Alteração da coloração da pele<br>• Aparecimento ou aumento de manchas<br>• Coceira',
      buttons: ['Fechar']
    });
    
    
    await alert.present();
  }
 
  async sintomasAlteracoesP() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'sintomas',
      subHeader: '',
      message: '• É o aparecimento da linha de coloração escura no centro do abdome devido à alteração do hormônio da gravidez<br>• É o aparecimento ou aumento das manchas na região da face, principalmente em “seios da face”',
      buttons: ['Fechar']
    });

    await alert.present();
  }
  async sintomasAlteracoesPConduta() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'conduta',
      subHeader: '',
      message: '• Beba bastante água<br>• Evite ganhar muito peso durante a gravidez<br>• Pratique atividades físicas<br>• Mantenha a pele hidratada<br>• Use filtro solar e repelentes',
      buttons: ['Fechar']
    });

    await alert.present();
  }
  async sintomasAlteracoesPAlerta() {
    const alert = await this.alertCtrl.create({
      header: 'Sinais de alerta:',
      cssClass: 'alerta',
      subHeader: '',
      message: '• Exantema ou manchas avermelhadas<br>• Aparecimento ou aumento de manchas: suspeita de rubéola ou sarampo<br>• Picadas de insetos',
      buttons: ['Fechar']
    });

    await alert.present();
  }
}