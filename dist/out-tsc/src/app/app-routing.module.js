var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuard } from './core/auth/auth.guard';
var routes = [
    { path: '', loadChildren: './pages/login/login.module#LoginPageModule', canActivate: [AuthGuard] },
    { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
    { path: 'about', loadChildren: './pages/about/about.module#AboutPageModule' },
    { path: 'edit-profile', loadChildren: './pages/edit-profile/edit-profile.module#EditProfilePageModule' },
    { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
    { path: 'mama', loadChildren: './pages/mama/mama.module#MamaPageModule' },
    { path: 'agf', loadChildren: './pages/agf/agf.module#AgfPageModule' },
    { path: 'info', loadChildren: './pages/info/info.module#InfoPageModule' },
    { path: 'info/seccaopecas', loadChildren: './pages/info/seccao_pecas/info.seccaopecas.module#InfoSeccaoPecasModule' },
    { path: 'info/regras', loadChildren: './pages/info/regras/info.regras.module#InfoRegrasModule' },
    { path: 'info/macroscopica', loadChildren: './pages/info/macroscopica/info.macroscopica.module#InfoMacroscopicaModule' },
    { path: 'info/fragmentos', loadChildren: './pages/info/fragmentos/info.fragmentos.module#InfoFragmentosModule' },
    { path: 'info/fotograficos', loadChildren: './pages/info/fotografico/info.fotografico.module#InfoFotograficosModule' },
    { path: 'info/etapasfundamentais', loadChildren: './pages/info/etapas_fundamentais/info.etapasfundamentais.module#InfoEtapasFundamentaisModule' },
    { path: 'info/desctecidos', loadChildren: './pages/info/descartes_tecidos/info.desctecidos.module#InfoDescTecidosModule' },
    { path: 'info/descalcificacao', loadChildren: './pages/info/descalcificacao/info.descalcificacao.module#InfoDescalcificacaoModule' },
    { path: 'info/acondicionamento', loadChildren: './pages/info/acondicionamento/info.acondicionamento.module#InfoAcondicionamentoModule' },
    { path: 'info/edit', loadChildren: './pages/info/form/info.form.module#InfoFormPageModule' },
    { path: 'ref-bib', loadChildren: './pages/ref-bib/ref-bib.module#RefBibPageModule' },
    { path: 'contacts', loadChildren: './pages/contacts/contacts.module#ContactsPageModule' },
    { path: 'tutorial', loadChildren: './pages/tutorial/tutorial.module#TutorialPageModule' },
    { path: 'biopsia', loadChildren: './pages/biopsia/biopsia.module#BiopsiaPageModule' },
    { path: 'biopsia_excisional', loadChildren: './pages/biopsia_excisional/biopsia_excisional.module#BiopsiaExcisionalPageModule' },
    { path: 'carcinoma', loadChildren: './pages/carcinoma/carcinoma.module#CarcinomaPageModule' },
    { path: 'cones_cervicais', loadChildren: './pages/cones_cervicais/cones_cervicais.module#ConesCervicaisPageModule' },
    { path: 'core_biopsy', loadChildren: './pages/core_biopsy/core_biopsy.module#CoreBiopsyPageModule' },
    { path: 'curetagens', loadChildren: './pages/curetagens/curetagens.module#CuretagensPageModule' },
    { path: 'ginecomastia', loadChildren: './pages/ginecomastia/ginecomastia.module#GinecomastiaPageModule' },
    { path: 'hiperplasia', loadChildren: './pages/hiperplasia/hiperplasia.module#HiperplasiaPageModule' },
    { path: 'implantes', loadChildren: './pages/implantes/implantes.module#ImplantesPageModule' },
    { path: 'laqueadura', loadChildren: './pages/laqueadura/laqueadura.module#LaqueaduraPageModule' },
    { path: 'mastectomia', loadChildren: './pages/mastectomia/mastectomia.module#MastectomiaPageModule' },
    { path: 'instrucoes_gerais', loadChildren: './pages/instrucoes_gerais/instrucoes_gerais.module#InstrucoesGeraisPageModule' },
    { path: 'mastoplastia', loadChildren: './pages/mastoplastia/mastoplastia.module#MastoplastiaPageModule' },
    { path: 'ooforectomia', loadChildren: './pages/ooforectomia/ooforectomia.module#OoforectomiaPageModule' },
    { path: 'ooforectomia/fotos', loadChildren: './pages/ooforectomia/fotos/ooforectomia.fotos.module#OoforectomiaFotosModule' },
    { path: 'ooforectomia/images', loadChildren: './pages/ooforectomia/imagens/ooforectomia.images.module#OoforectomiaImagesModule' },
    { path: 'quimioterapia', loadChildren: './pages/quimioterapia/quimioterapia.module#QuimioterapiaPageModule' },
    { path: 'reexcisao', loadChildren: './pages/reexcisao/reexcisao.module#ReexcisaoPageModule' },
    { path: 'salpingectomia', loadChildren: './pages/salpingectomia/salpingectomia.module#SalpingectomiaPageModule' },
    { path: 'vulvectomia', loadChildren: './pages/vulvectomia/vulvectomia.module#VulvectomiaPageModule' },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        NgModule({
            imports: [RouterModule.forRoot(routes)],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map