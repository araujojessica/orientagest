var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
var PlatformDectorService = /** @class */ (function () {
    function PlatformDectorService(platformId) {
        this.platformId = platformId;
    }
    PlatformDectorService.prototype.isPlataformBrowser = function () {
        return isPlatformBrowser(this.platformId);
    };
    PlatformDectorService = __decorate([
        Injectable({ providedIn: 'root' }),
        __param(0, Inject(PLATFORM_ID)),
        __metadata("design:paramtypes", [String])
    ], PlatformDectorService);
    return PlatformDectorService;
}());
export { PlatformDectorService };
//# sourceMappingURL=plataform-dector.service.js.map