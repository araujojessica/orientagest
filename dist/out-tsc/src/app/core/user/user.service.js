var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { TokenService } from '../token/token.service';
import { BehaviorSubject } from 'rxjs';
import * as jwt_decode from 'jwt-decode';
var UserService = /** @class */ (function () {
    function UserService(tokenService) {
        this.tokenService = tokenService;
        this.userSubject = new BehaviorSubject(null);
        this.tokenService.hasToken() && this.decodeAndNotify();
    }
    UserService.prototype.setToken = function (token) {
        this.tokenService.setToken(token);
        this.decodeAndNotify();
    };
    UserService.prototype.getUser = function () {
        return this.userSubject.asObservable();
    };
    UserService.prototype.decodeAndNotify = function () {
        var token = this.tokenService.getToken();
        var user = jwt_decode(token);
        this.userName = user.name;
        this.fullName = user.fullName;
        this.userSubject.next(user);
    };
    UserService.prototype.logout = function () {
        this.tokenService.removeToken();
        this.userSubject.next(null);
    };
    UserService.prototype.isLogged = function () {
        return this.tokenService.hasToken();
    };
    UserService.prototype.getUserName = function () {
        return this.userName;
    };
    UserService.prototype.getFullName = function () {
        return this.fullName;
    };
    UserService = __decorate([
        Injectable({ providedIn: 'root' }),
        __metadata("design:paramtypes", [TokenService])
    ], UserService);
    return UserService;
}());
export { UserService };
//# sourceMappingURL=user.service.js.map