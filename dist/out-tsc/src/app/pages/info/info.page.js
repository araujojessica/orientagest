var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { UserService } from 'src/app/core/user/user.service';
import { InfoService } from './service/info.service';
import { Router } from '@angular/router';
var InfoPage = /** @class */ (function () {
    function InfoPage(userService, infoService, router) {
        var _this = this;
        this.userService = userService;
        this.infoService = infoService;
        this.router = router;
        this.infos = [];
        this.user$ = userService.getUser();
        infoService.listFromInfo().subscribe(function (infos) {
            console.log(infos);
            _this.infos = infos;
        });
    }
    InfoPage.prototype.seccoes = function () {
        this.router.navigateByUrl('/info/etapasfundamentais');
    };
    InfoPage.prototype.acondicionamento = function () {
        this.router.navigateByUrl('/info/acondicionamento');
    };
    InfoPage.prototype.macroscopica = function () {
        this.router.navigateByUrl('/info/macroscopica');
    };
    InfoPage.prototype.seccao_pecas = function () {
        this.router.navigateByUrl('/info/seccaopecas');
    };
    InfoPage.prototype.fragmentos = function () {
        this.router.navigateByUrl('/info/fragmentos');
    };
    InfoPage.prototype.descalcificacao = function () {
        this.router.navigateByUrl('/info/descalcificacao');
    };
    InfoPage.prototype.regras = function () {
        this.router.navigateByUrl('/info/regras');
    };
    InfoPage.prototype.fotografico = function () {
        this.router.navigateByUrl('/info/fotograficos');
    };
    InfoPage.prototype.descarte_tecido = function () {
        this.router.navigateByUrl('/info/desctecidos');
    };
    InfoPage = __decorate([
        Component({
            selector: 'app-info',
            templateUrl: './info.page.html',
            styleUrls: ['./info.page.scss'],
        }),
        __metadata("design:paramtypes", [UserService, InfoService, Router])
    ], InfoPage);
    return InfoPage;
}());
export { InfoPage };
//# sourceMappingURL=info.page.js.map